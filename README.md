# Emotion recognition with deep learning

In project is used fer2013.csv Database, a set of 35,000 pictures of people displaying 7 emotions:
    - angry
    - disgusted
    - fearful
    - happy
    - sad
    - surprised
    - neutral

Database can be download from Kaggle.

Used libraries:
    - NumPy (numpy)
    - SciPy (scipy)
    - OpenCV (opencv-python)
    - PIL (pillow)
    - Pandas (pandas)